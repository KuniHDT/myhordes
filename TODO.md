# NIGHTWATCH
 * GUI
 * Watch / Unwatch
 * NightlyHandler stuff
 * Le chantier "Lance-Bête" empêche les terrorisés en veille :slight_smile:

Stats :
    Death : 5% chance, 1% for guardian
    Terror : 6% chance ? [TOBECONFIRMED]

    Increase death by 10% each watched night
    Decrease death by 5% each non wathed night

    Injured chances are death + 5% chances in normal, +20% chance in panda
 * Mit der gestrigen Horde kamen %count% der ehemaligen Einwohner zurück, die in der Außenwelt starben; zähneknirschend und hungrig...
 * In last night's horde, %count% former residents who died in the World Beyond came back, gnashing their teeth and looking famished... ?
 * %count% watchman is posted at the limit of the town to stop the zombie attack. / %count% watchman are posted at the limit of the town to stop the zombie attack.
 * %count% Nachtwächter ist/sind auf der Stadtmauer, um den Zombieangriff zu stoppen.

* Nur ein suizidaler Nachtwächter bewacht das Stadttor diese Nacht! Wünscht %citizen% einen schnellen Tod. / %count% suizidale Nachtwächter bewachen heute Nacht das Stadttor! Wünscht %citizen_list% einen schnellen Tod.
* Only one suicidal maniac is watching the gates tonight! Wish %citizen% a quick death. / %count% suicidal maniacs arewatching the gates tonight! Wish %citizen_list% a quick death.

* Nachtwächter %citizen% hat bis zur Erschöpfung gekämpft, dieser Zombieangriff hat ihn schwer gezeichnet, der Horror steht ihm ins Gesicht geschrieben.
* Watchman %citizen% resisted all they could, but this zombie onslaught etched a look of total terror on their face! 

* Die Nachtwächter haben mutig die Stadt verteidigt.
* The watchmen courageously defended the town!